(let ((bootstrap-file (concat user-emacs-directory "straight/repos/straight.el/bootstrap.el"))
      (bootstrap-version 3))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

(setq inhibit-startup-buffer-menu t)
(setq inhibit-startup-screen t)
(setq initial-buffer-choice t)
(setq initial-scratch-message "")
(setq load-prefer-newer t)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(use-package simple
  :config (column-number-mode)
  :straight nil)

(use-package paren
  :config (show-paren-mode))

(use-package linum
  :config (global-linum-mode))

(setq visible-bell t)			; Disables bell on error and flashes screen instead

(use-package hydra)

(use-package company                    ; Graphical (auto-)completion
  :init
  (global-company-mode)
  :config
  (setq
   company-tooltip-align-annotations t
   company-tooltip-flip-when-above t
   ;; Easy navigation to candidates with M-<n>
   company-show-numbers t
   company-minimum-prefix-length 3
   company-idle-delay 1)
  :bind (:map company-active-map
              ;; Use C-n and C-p to navigate suggestions
              ("C-n" . company-select-next)
              ("C-p" . company-select-previous)))

(use-package ivy                        ; Minibuffer completion
  :defer t
  :init (ivy-mode 1)
  :bind (
         ("C-c b r" . ivy-resume)
         ("C-s" . swiper))
  :config
  ;; Include recentf and bookmarks to switch buffer, and tune the count format.
  (setq ivy-use-virtual-buffers t
        ivy-count-format "(%d/%d) "
        enable-recursive-minibuffers t
        ivy-re-builders-alist
        '((t . ivy--regex-fuzzy)))      ;Uses flx-more matches, better sorting
  :diminish ivy-mode)

(use-package which-key
  :config (which-key-mode))

(use-package elpy
  :defer t
  :init
  (add-hook 'python-mode-hook #'elpy-mode)
  (setq elpy-rpc-backend "jedi")
  (setq elpy-dedicated-shells nil))

(use-package ein
  :config
  (setq ein:completion-backend 'ein:use-company-backend)
  (add-hook 'ein:notebook-mode-hook
               '(defhydra hydra-ein (ein:notebook-mode-map "C-c")
                 "EIN Navigate"
                 ("n" ein:worksheet-goto-next-input "Next Cell")
                 ("p" ein:worksheet-goto-prev-input "Previous Cell"))))
  ;; :config (with-eval-after-load "ein-notebook"
  ;; 	    (defhydra hydra-ein (ein:notebook-mode-map "C-c")
  ;; 	      "EIN Navigate"
  ;; 	      ("n" ein:worksheet-goto-next-input "Next Cell")
  ;; 	      ("p" ein:worksheet-goto-prev-input "Previous Cell")))
;;
;)

(use-package lispy
  :init
  (add-hook 'emacs-lisp-mode-hook '(lambda () (lispy-mode 1))))

(use-package markdown-mode)

(use-package magit
  :defer t
  :bind (("C-x g"   . magit-status)
         ("C-x M-g" . magit-dispatch-popup))
  :config
  (magit-add-section-hook 'magit-status-sections-hook
                        'magit-insert-modules
                        'magit-insert-stashes
                        'append))
(use-package git)

(use-package org
                                        ;  :straight org
  :init
  (defun org-git-version ()
    "The Git version of org-mode.
  Inserted by installing org-mode or when a release is made."
    (require 'git)
    (let ((git-repo (expand-file-name
                     "straight/repos/org/" user-emacs-directory)))
      (string-trim
       (git-run "describe"
                "--match=release\*"
                "--abbrev=6"
                "HEAD"))))

  (defun org-release ()
    "The release version of org-mode.
  Inserted by installing org-mode or when a release is made."
    (require 'git)
    (let ((git-repo (expand-file-name
                     "straight/repos/org/" user-emacs-directory)))
      (string-trim
       (string-remove-prefix
        "release_"
        (git-run "describe"
                 "--match=release\*"
                 "--abbrev=0"
                 "HEAD")))))

  (provide 'org-version)
  (add-hook 'org-mode-hook #'visual-line-mode) ;Wraps text based on word boundries
  :bind (
         ("C-c c" . org-capture)
         ("C-c a" . org-agenda)
         ("C-c l" . org-store-link))
  :config
  (setq org-startup-indented t)		; Cleaner look
  (setq org-log-done 'note)
  (setq org-log-into-drawer t)
  (setq org-todo-keywords '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "|" "DONE(d)")))
  (progn
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((emacs-lisp . t)
       (python . t)
       (shell . t))))
  (setq org-agenda-files '("~/gtd/inbox.org"
                           "~/gtd/gtd.org"
                           "~/gtd/tickler.org"))
  (setq org-refile-targets '(("~/gtd/gtd.org" :maxlevel . 3)
                             ("~/gtd/someday.org" :level . 1)
                             ("~/gtd/tickler.org" :maxlevel . 2)))
  ;; Org Capture
  (setq org-default-notes-file "~/gtd/inbox.org")
  (setq org-capture-templates
        '(("t" "Todo" entry (file+headline "~/gtd/inbox.org" "Inbox")
           "* TODO %?")))
  (require 'org-tempo))

 (use-package org-bullets
  :commands (org-bullets-mode)
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package org-ref
  :config
  (setq org-ref-bibliography-notes "~/MEGAsync/bibliography/notes.org"
        org-ref-default-bibliography "~/MEGAsync/bibliography/references.bib"
        org-ref-pdf-directory "~/MEGAsync/bibliography/pdfs/"))

(use-package ivy-bibtex
  :config
  (setq bibtex-completion-bibliography "~/MEGAsync/bibliography/references.bib"
        bibtex-completion-library-path "~/MEGAsync/bibliography/pdfs"
        bibtex-completion-notes-path "~/MEGAsync/bibliography/notes.org"))
(use-package interleave)

(use-package org-noter)

(use-package ace-window
  :defer t
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :bind*
  ("M-p" . ace-window))

(use-package spacemacs-theme
  :defer t
  :init (load-theme 'spacemacs-dark t))

(add-to-list 'default-frame-alist
             '(font . "Source Code Pro-10:weight=normal:width=normal"))

(use-package nov
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))
